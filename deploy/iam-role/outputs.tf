output "role_name" {
  description = "The name of the role"
  value = aws_iam_role.gitlab_role.name
}

output "role_arn" {
  description = "The arn of the role"
  value = aws_iam_role.gitlab_role.name
}