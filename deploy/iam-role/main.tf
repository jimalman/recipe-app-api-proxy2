resource "aws_iam_role" "gitlab_role" {
  name = "aws-recipe-app-gitlab-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::438028467474:oidc-provider/gitlab.com"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
            "gitlab.com:sub": "project_path:jimalman/gitlab-oidc-connect:ref_type:branch:ref:main",
            "gitlab.com:aud": "https://gitlab.com"
        }
      }
    }
  ]
}
EOF
}