resource "aws_ecr_repository" "repository" {
  name                 = "recipe-app-api"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_iam_policy" "policy" {
  name        = "RecipeAppApiPushECR"
  description = "Push ECR policy"

  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ec2:*",
        ]
        Effect   = "Allow"
        Resource = aws_ecr_repository.repository.arn
      },
      {
        Effect  = "Allow"
        Action  = [
          "ecr:GetAuthrorizationToken",
        ]
        Resource = "*"
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "attach" {
  role       = var.iam_role_name
  policy_arn = aws_iam_policy.policy.arn
}
